
project "template"
	kind "ConsoleApp"
	language "C++"
	files { "src/**.h", "src/**.cpp" }
	objdir "obj"
	targetname "template"

	includedirs ("../../cglib/src")
	libdirs ("../../cglib/lib")


	-- untested
	configuration "windows"
		defines "WIN32"
		links {"glu32", "opengl32", "gdi32", "winmm", "user32"}

	configuration "linux"
		links {"GL", "X11"}


	configuration "release"
		flags { "ExtraWarnings" , "Optimize" }
		objdir "obj/release"
		targetdir "bin/release"
		links {"cg"}

	configuration "debug"
		flags { "ExtraWarnings" , "Symbols" }
		objdir "obj/debug"
		targetdir "bin/debug"
		links {"cgD"}


	configuration "freeglut"
		libdirs { gmainLibDir..freeglut.."/lib" }

	configuration{ "freeglut" , "debug" }
		links {"freeglutD"}

	configuration{ "freeglut" , "release" }
		links {"freeglut"}

	configuration{ "freeglut" , "linux" }
		links { "Xxf86vm", "Xrandr", "rt", "Xi" }


	configuration "glfw"
		libdirs { gmainLibDir..glfw.."/library" }

	configuration{ "glfw" , "debug" }
		links {"glfwD"}

	configuration{ "glfw" , "release" }
		links {"glfw"}


	configuration "glload"
		libdirs { gmainLibDir..glload.."/lib" }

	configuration{ "glload" , "debug" }
		links {"glloadD"}

	configuration{ "glload" , "release" }
		links {"glload"}


	configuration "glew"
		libdirs { gmainLibDir..glew.."/lib" }
		links {"GLEW"}


