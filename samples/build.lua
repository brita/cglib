--build samples
local projDir = "./"
mainDir = "../../"
smainLibDir = mainDir..mainLibDir

local projectPremakes =
{
	projDir.."simple_window/",
	projDir.."simple_window_app/",
	projDir.."lab2/",
	--projDir.."lab4/",
	--projDir.."projection_math/",
	--projDir.."projection_glm/",
}

-- generic configuration options (may be used in every sample)

debug_and_release_configuration = function()

	configuration "release"
		defines { "NDEBUG" }
		flags { "Optimize" }
		objdir "obj/release"
		targetdir "bin/release"

	configuration "debug"
		defines { "DEBUG" }
		flags { "Symbols" }
		objdir "obj/debug"
		targetdir "bin/debug"

end

os_specific_configuration = function()

	configuration "windows" --untested
		defines "WIN32"
		links {"glu32", "opengl32", "gdi32", "winmm", "user32"}

	configuration "linux"
		links {"GL", "X11"}

end

use_cglib = function()

	configuration {}
		includedirs (mainDir.."cglib/src")
		libdirs (mainDir.."cglib/lib")

	configuration "release"
		links {"cg"}

	configuration "debug"
		links {"cgD"}

end

use_freeglut = function()

	configuration {}
		libdirs (smainLibDir..freeglut.."/lib")

	configuration "linux"
		links { "Xxf86vm", "Xrandr", "rt", "Xi" }

	configuration "release"
		links {"freeglut"}

	configuration "debug"
		links {"freeglutD"}

end

use_glload = function()

	configuration "glload"
		libdirs { smainLibDir..glload.."/lib" }

	configuration{ "glload" , "debug" }
		links {"glloadD"}

	configuration{ "glload" , "release" }
		links {"glload"}

end

use_glew = function()

	configuration "glew"
		libdirs { smainLibDir..glew.."/lib" }
		links {"GLEW"}

end




--generate builds for the samples
for i, pdir in ipairs(projectPremakes) do
	luaPremake = pdir.."build.lua"
	if(#os.matchfiles(luaPremake) ~= 0) then
		dofile(luaPremake)
	end
end
