#include "InputController.h"

namespace swa
{

	InputController::InputController(std::string id) : cg::Entity(id) {}

	InputController::~InputController() {}


	void InputController::init() {}


	void InputController::onKeyPressed(unsigned char key)
	{
		if (key == 27) //Esc
			cg::Manager::instance()->shutdownApp();
	}

	void InputController::onKeyReleased(unsigned char key)
	{

	}

	void InputController::onSpecialKeyPressed(int key)
	{

	}

	void InputController::onSpecialKeyReleased(int key)
	{

	}

}
