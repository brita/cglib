#ifndef TRIANGLE_H_
#define TRIANGLE_H_

#include "cg/gl.h"
#include "cg/Entity.h"
#include "cg/IDrawListener.h"
#include "cg/IUpdateListener.h"
#include "cg/ShaderUtils.h"
#include "cg/Util.h"


namespace swa
{

	class Triangle : public cg::Entity, public cg::IDrawListener, public cg::IUpdateListener
	{
		private:
			GLuint _vertexBufferObject, _vertexArrayObject;
			GLuint _program;
			GLuint _elapsedTimeUniform;

		public:
			Triangle(std::string id);
			virtual ~Triangle();

			void init();
			void draw();
			void update(float elapsedMillis);
	};

}

#endif // TRIANGLE_H_
