#ifndef INPUTCONTROLLER_H_
#define INPUTCONTROLLER_H_

#include "cg/Entity.h"
#include "cg/IKeyboardEventListener.h"
#include "cg/Manager.h"


namespace swa
{

	class InputController : public cg::Entity, public cg::IKeyboardEventListener
	{
		public:
			InputController(std::string id);
			virtual ~InputController();

			void init();

			void onKeyPressed(unsigned char key);
			void onKeyReleased(unsigned char key);
			void onSpecialKeyPressed(int key);
			void onSpecialKeyReleased(int key);
	};

}

#endif // INPUTCONTROLLER_H_
