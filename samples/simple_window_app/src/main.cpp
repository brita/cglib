#include "cg/cg.h"

#include "App.h"

int main(int argc, char** argv)
{
	cg::Manager::instance()->runApp(new swa::App(), argc, argv);
	return 0;
}
