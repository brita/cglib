#include "App.h"

namespace swa
{

	App::App()
	{
		_windowInfo._caption = "Application Hello Triangle";
	}

	App::~App() {}


	void App::createEntities()
	{
		addEntity(new InputController("input"));
		addEntity(new Triangle("triangle"));
		//addEntity(new GameCamera("game_camera"));
	}


	void App::createViews()
	{
		cg::View* view = createView("view");
		view->setViewport(0.5f,0.3f,1.0f,1.0f);
		view->setClearColor(1.0f, 0.0f, 0.0f, 0.0f);
		//view->linkEntityAtEnd("debug_camera");
		view->linkEntityAtEnd("triangle");

		cg::View* view2 = createView("view2");
		view2->setViewport(0.0f,0.1f,0.5f,0.2f);
		view2->setClearColor(1.0f, 0.0f, 1.0f, 0.0f);
	}
}
