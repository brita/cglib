#ifndef APP_H_
#define APP_H_

#include "cg/Application.h"

#include "InputController.h"
#include "Triangle.h"


namespace swa
{

	class App : public cg::Application
	{
		public:
			App();
			virtual ~App();

			virtual void createEntities();
			virtual void createViews();
	};

}

#endif // APP_H_
