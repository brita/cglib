#include "App.h"

#include <vector>
#include <algorithm>


namespace sw
{

	App::App()
	{
		_windowInfo._caption = "Basic Hello Triangle";
	}

	App::~App()
	{
		glDeleteProgram(_program);
		//gldelete buffers
	}



	const GLfloat vertexData[] =
	{
		 0.25f,  0.25f, 0.0f, 1.0f,
		 0.25f, -0.25f, 0.0f, 1.0f,
		-0.25f, -0.25f, 0.0f, 1.0f,
	};

	void App::onInit()
	{
		BasicApplication::onInit();

		// creating memory on the GPU to hold the vertex data
		glGenBuffers(1, &_vertexBufferObject);

		glBindBuffer(GL_ARRAY_BUFFER, _vertexBufferObject);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);


		// creating the program
		std::vector<GLuint> shaderList;

		shaderList.push_back(cg::ShaderUtils::instance()->LoadShader(GL_VERTEX_SHADER, "src/shaders/moving.vert"));
		shaderList.push_back(cg::ShaderUtils::instance()->LoadShader(GL_FRAGMENT_SHADER, "src/shaders/simple.frag"));

		_program = cg::ShaderUtils::instance()->CreateProgram(shaderList);

		std::for_each(shaderList.begin(), shaderList.end(), glDeleteShader);


		// uniforms setup
		GLuint loopDurationUniform = glGetUniformLocation(_program, "loopDuration"); // this uniform will be set only once
		glUseProgram(_program);
		glUniform1f(loopDurationUniform, 4.0f);
		glUseProgram(0);

		_elapsedTimeUniform = glGetUniformLocation(_program, "timeElapsed"); // this uniform will be set at every frame


		// creating and binding a vao
		glGenVertexArrays(1, &_vertexArrayObject);
		glBindVertexArray(_vertexArrayObject);
	}


	void App::onDisplay()
	{
		// binding the program shader
		glUseProgram(_program);

		// bindind the vertex data
		glBindBuffer(GL_ARRAY_BUFFER, _vertexBufferObject);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0); // feed 4 GL_FLOAT per vertex shader

		// drawing the data in triangle form
		glDrawArrays(GL_TRIANGLES, 0, 3);

		// unbinding vertex data
		glDisableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// unbinding program
		glUseProgram(0);
	}


	void App::onReshape(int w, int h)
	{
		glViewport(0, 0, (GLsizei) w, (GLsizei) h);
	}

	void App::onUpdate()
	{
		glUseProgram(_program);
		glUniform1f(_elapsedTimeUniform, cg::Util::instance()->getAppTime()); // time since the app begun
		glUseProgram(0);
	}
}
