#ifndef APP_H_
#define APP_H_

#include "cg/BasicApplication.h"
#include "cg/ShaderUtils.h"


namespace sw
{

	class App : public cg::BasicApplication
	{
		private:
			GLuint _vertexBufferObject, _vertexArrayObject;
			GLuint _program;
			GLuint _elapsedTimeUniform;

		public:
			App();
			virtual ~App();


			void onInit();
			void onDisplay();
			void onReshape(int w, int h);
			void onUpdate();
	};

}

#endif // APP_H_
