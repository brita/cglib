
project "simple_window"
	kind "ConsoleApp"
	language "C++"
	files { "src/**.h", "src/**.cpp" }
	targetname "simple_window"


	os_specific_configuration()

	debug_and_release_configuration()

	use_cglib()
	use_freeglut()
	use_glload()
	use_glew()
