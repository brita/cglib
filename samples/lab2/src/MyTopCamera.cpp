#include "MyTopCamera.h"

namespace lab2
{

	MyTopCamera::MyTopCamera(const std::string& id) : Entity(id) {}

	MyTopCamera::~MyTopCamera() {}

	void MyTopCamera::init()
	{
		_projectionMatrix = glm::mat4(1.0f);

		glGenBuffers(1, &_projectionMatrixUBO);

		glBindBuffer(GL_UNIFORM_BUFFER, _projectionMatrixUBO);
		glBufferData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), NULL, GL_STREAM_DRAW);
		glBindBuffer(GL_UNIFORM_BUFFER, 0);

		glBindBufferRange(GL_UNIFORM_BUFFER, globalMatricesBindingIndex, _projectionMatrixUBO, 0, sizeof(GLfloat)*16);
	}

	void MyTopCamera::draw()
	{
//        glMatrixMode(GL_PROJECTION);
//        glLoadIdentity();
//        glOrtho(0,_winWidth,0,_winHeight,0,-100);
//        glMatrixMode(GL_MODELVIEW);
//        glLoadIdentity();

		//_projectionMatrix = glm::ortho(0,_winWidth,0,_winHeight,0,-100);

		_projectionMatrix[0][0] = 2.0f/(_winWidth-0);
		_projectionMatrix[1][1] = 2.0f/(_winHeight-0);
		_projectionMatrix[2][2] = 2.0f/(-100-0);
		_projectionMatrix[3][3] = 1.0f;
		_projectionMatrix[3][0] = -((float)(_winWidth+0))/((float)(_winWidth-0));
		_projectionMatrix[3][1] = -((float)(_winHeight+0))/((float)(_winHeight-0));
		_projectionMatrix[3][2] = -(-100+0)/(-100-0);

		glBindBuffer(GL_UNIFORM_BUFFER, _projectionMatrixUBO);
		glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4), glm::value_ptr(_projectionMatrix));
		glBindBuffer(GL_UNIFORM_BUFFER, 0);
	}

	void MyTopCamera::onReshape(int width, int height)
	{
		_winWidth = width;
		_winHeight = height;
	}

}
