#version 330

layout(location = 0) in vec2 position;
layout(location = 1) in vec4 color;

smooth out vec4 theColor;

uniform mat4 modelMat;

layout(std140) uniform GlobalMatrices
{
	mat4 projectionMatrix;
};

void main()
{
	gl_Position = projectionMatrix * modelMat * vec4(position, 0.0f, 1.0f);
	theColor = color;
}
