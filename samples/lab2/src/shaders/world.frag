#version 330

out vec4 outputColor;
uniform vec3 rgbColor;

void main()
{
	outputColor = vec4(rgbColor, 1.0f);
}
