#ifndef MY_RECTANGLE_H
#define MY_RECTANGLE_H

#include <string>
#include "cg/cg.h"
#include "MyTopCamera.h"

namespace lab2
{
	class MyRectangle : public cg::Entity, public cg::IDrawListener, public cg::IUpdateListener
	{
		private:
			GLuint _matUnif;
			GLuint _vertexBufferObject, _vertexArrayObject;
			GLuint _program;
			glm::vec2 _position, _direction;
			glm::mat4 _modelMatrix;
			float _velocity, _acceleration, _stopped;
			float _angle, _angularVelocity;
			void rotateDeg(glm::vec2 &vec, float angle);

		public:
			MyRectangle(std::string id);
			~MyRectangle();

			void init();
			void update(float elapsed_millis);
			void draw();
	};
}

#endif
