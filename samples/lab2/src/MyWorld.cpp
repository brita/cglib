#include "MyWorld.h"

#include <algorithm>


namespace lab2
{

	MyWorld::MyWorld(const std::string& id) : cg::Entity(id)
	{
		GLfloat border = 0.01f;
		GLfloat v = 1.0f - border;

		// vertex data
		const GLfloat vertexData[] =
		{
			-v, -v, //1
			-v,  v, //2
			 v,  v, //3
			 v, -v, //4
			-v, -v, //1
			 v,  v, //3
		};

		// creating memory on the GPU to hold the vertex data
		glGenBuffers(1, &_vertexBufferObject);

		glBindBuffer(GL_ARRAY_BUFFER, _vertexBufferObject);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);


		// creating the program
		std::vector<GLuint> shaderList;
		shaderList.push_back(cg::ShaderUtils::instance()->LoadShader(GL_VERTEX_SHADER, "src/shaders/world.vert"));
		shaderList.push_back(cg::ShaderUtils::instance()->LoadShader(GL_FRAGMENT_SHADER, "src/shaders/world.frag"));

		_program = cg::ShaderUtils::instance()->CreateProgram(shaderList);

		std::for_each(shaderList.begin(), shaderList.end(), glDeleteShader);


		// setting up uniforms
		_colorUnif = glGetUniformLocation(_program, "rgbColor");
		glUseProgram(_program);
		glUniform3f(_colorUnif, 1.0f, 1.0f, 1.0f);
		glUseProgram(0);


		// creating and binding a vao
		glGenVertexArrays(1, &_vertexArrayObject);
		glBindVertexArray(_vertexArrayObject);
	}

	MyWorld::~MyWorld() {}

	void MyWorld::init() {}

	
	void MyWorld::draw()
	{
		// back-face culling
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glFrontFace(GL_CW);


		// binding the program shader
		glUseProgram(_program);

		// bindind the vertex data
		glBindBuffer(GL_ARRAY_BUFFER, _vertexBufferObject);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0); // feed 2 GL_FLOAT per vertex shader

		// drawing the data in triangle form
		glDrawArrays(GL_TRIANGLES, 0, 6);

		// unbinding vertex data
		glDisableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// unbinding program
		glUseProgram(0);

		// end back-face culling
		glDisable(GL_CULL_FACE);
	}


	void MyWorld::setColor(float r, float g, float b)
	{
		glUseProgram(_program);
		glUniform3f(_colorUnif, r, g, b);
		glUseProgram(0);
	}

}
