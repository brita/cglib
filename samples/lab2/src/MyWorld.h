#ifndef MY_WORLD_H
#define MY_WORLD_H

#include "cg/cg.h"

namespace lab2
{

	class MyWorld : public cg::Entity, public cg::IDrawListener
	{
		private:
			GLuint _vertexBufferObject, _vertexArrayObject;
			GLuint _program;
			GLuint _colorUnif;

		public:
			MyWorld(const std::string& id);
			~MyWorld();

			void init();
			void draw();
			void setColor(float r, float g, float b);
	};
}

#endif
