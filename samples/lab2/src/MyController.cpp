#include "MyController.h"

namespace lab2
{

	MyController::MyController(const std::string& id) : cg::Entity(id) {}
	MyController::~MyController() {}

	void MyController::init() {}

	
	void MyController::onKeyPressed(unsigned char key)
	{
		if (key == 27)
			cg::Manager::instance()->shutdownApp();
	}

	void MyController::onKeyReleased(unsigned char key)
	{
		if(key == 'm')
		{
			// Shows how to turn on and off views
			if(cg::ViewNotifier::instance()->exists("view2"))
			{
				cg::ViewNotifier::instance()->remove("view2");
			}
			else
			{
				cg::View* v = (cg::View*)cg::Registry::instance()->get("view2");
				cg::ViewNotifier::instance()->addAtEnd(v);
			}
		}
		else if(key == 'a')
		{
			// shows how to add new entities dynamically to the scene 
			if(cg::Registry::instance()->exists("Rectangle1") == false)
			{
				MyRectangle* r = new MyRectangle("Rectangle1");
				cg::Registry::instance()->add(r);
				cg::View *v = (cg::View *)cg::Registry::instance()->get("view1");
				v->linkEntityAtEnd("Rectangle1");
				r->init(); // init is not called by default (as it was with cg::Application::addEntity).
			}
		}
		else if(key == 'r')
		{
			if(cg::Registry::instance()->exists("Rectangle1"))
			{
				// shows how to remove entities dynamically to the scene 
				cg::View *v = (cg::View *)cg::Registry::instance()->get("view1");
				v->unlinkEntity("Rectangle1");
				MyRectangle *r = (MyRectangle *)cg::Registry::instance()->get("Rectangle1");
				cg::Registry::instance()->remove("Rectangle1");
				delete r;
			}
		}
		else if(key == 'd')
		{
			// shows the destruction of a cg::Entity while inside a callback iteration, in this case MyController itself.
			cg::Registry::instance()->destroy(_id);
		}
		else if(key == 'e')
		{
			// shows the destruction and the restoration of the full registry.
			cg::Registry::instance()->destroyAll();
			cg::Manager::instance()->getApp()->onInit(); 
			// calls createEntities, createViews, and init of all Entities
			cg::Manager::instance()->getApp()->onReshape(800,600); 
			// calls onReshape on all views so they have the correct width and heigth to compute camera transformations
		} 
	}

	void MyController::onSpecialKeyReleased(int key)
	{
		if (key == GLUT_KEY_F1)
			((cg::Application*)cg::Manager::instance()->getApp())->dump();
	}

}

