#include "MyApp.h"

namespace lab2
{

	MyApp::MyApp()
	{
		_windowInfo._caption = "Lab2";
		_windowInfo._width = 800;
		_windowInfo._height = 600;
	}

	MyApp::~MyApp() { }


	MyWorld* MyApp::createWorld(const std::string& name, float r, float g, float b)
	{
		MyWorld* w = new MyWorld(name);
		w->setColor(r,g,b);
		return w;
	}

	void MyApp::createEntities()
	{
		addEntity(new MyController("controller"));
		addEntity(createWorld("world0",0.1f,0.1f,0.3f));
		addEntity(createWorld("world1",0.1f,0.3f,0.1f));
		addEntity(createWorld("world2",0.3f,0.1f,0.1f));
		addEntity(new MyRectangle("Rectangle1"));
		addEntity(new MyTopCamera("camera"));
	}

	void MyApp::createViews()
	{
		cg::View* v0 = createView("view0");
		v0->setViewport(0.1f,0.1f,0.8f,0.8f);
		v0->linkEntityAtEnd("camera");
		v0->linkEntityAtEnd("world0");
		v0->linkEntityAtEnd("Rectangle1");

		cg::View* v1 = createView("view1");
		v1->setViewport(0.15f,0.15f,0.25f,0.25f);
		v1->linkEntityAtEnd("camera");
		v1->linkEntityAtEnd("world1");
		v1->linkEntityAtEnd("Rectangle1");

		cg::View* v2 = createView("view2");
		v2->setViewport(0.6f,0.15f,0.25f,0.25f);
		v2->linkEntityAtEnd("camera");
		v2->linkEntityAtEnd("world2");
		v2->linkEntityAtEnd("Rectangle1");
	}
}
