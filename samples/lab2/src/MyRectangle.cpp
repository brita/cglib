#include "MyRectangle.h"

namespace lab2
{

	MyRectangle::MyRectangle(std::string id) : cg::Entity(id) {}
	MyRectangle::~MyRectangle() {}

	void MyRectangle::init()
	{
		cg::Window* win = cg::Manager::instance()->getApp()->getWindowInfo();
		_position = glm::vec2(win->_width/2.0f, win->_height/2.0f);
		_direction = glm::vec2(1.0, 0.0);
		_velocity = 0.0;
		_acceleration = 100.0;
		_angle = 0.0;
		_angularVelocity = 100.0;

		// vertex data
		const GLfloat vertexData[] =
		{
			// glRect1
			-20.0f, -10.0f, //1
			-20.0f,  10.0f, //2
			 15.0f,  10.0f, //3
			 15.0f, -10.0f, //4
			-20.0f, -10.0f, //1
			 15.0f,  10.0f, //3

			 //	glRect2
			-15.0f, -10.0f, //1
			-15.0f,  10.0f, //2
			 20.0f,  10.0f, //3
			 20.0f, -10.0f, //4
			-15.0f, -10.0f, //1
			 20.0f,  10.0f, //3

			 0.8f, 0.8f, 0.2f, 1.0f,	 0.8f, 0.8f, 0.2f, 1.0f,	 0.8f, 0.8f, 0.2f, 1.0f,
			 0.8f, 0.8f, 0.2f, 1.0f,	 0.8f, 0.8f, 0.2f, 1.0f,	 0.8f, 0.8f, 0.2f, 1.0f,
			 0.5f, 0.5f, 0.2f, 1.0f,	 0.5f, 0.5f, 0.2f, 1.0f,	 0.5f, 0.5f, 0.2f, 1.0f,
			 0.5f, 0.5f, 0.2f, 1.0f,	 0.5f, 0.5f, 0.2f, 1.0f,	 0.5f, 0.5f, 0.2f, 1.0f,
		};

		// creating memory on the GPU to hold the vertex data
		glGenBuffers(1, &_vertexBufferObject);

		glBindBuffer(GL_ARRAY_BUFFER, _vertexBufferObject);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);


		// creating the program
		std::vector<GLuint> shaderList;
		shaderList.push_back(cg::ShaderUtils::instance()->LoadShader(GL_VERTEX_SHADER, "src/shaders/simple.vert"));
		shaderList.push_back(cg::ShaderUtils::instance()->LoadShader(GL_FRAGMENT_SHADER, "src/shaders/simple.frag"));

		_program = cg::ShaderUtils::instance()->CreateProgram(shaderList);

		std::for_each(shaderList.begin(), shaderList.end(), glDeleteShader);


		// setting up uniforms
		GLuint projectionMatrixUnif = glGetUniformBlockIndex(_program, "GlobalMatrices");
		glUniformBlockBinding(_program, projectionMatrixUnif, MyTopCamera::globalMatricesBindingIndex);
		_matUnif = glGetUniformLocation(_program, "modelMat");


		// creating and binding a vao
		glGenVertexArrays(1, &_vertexArrayObject);
		glBindVertexArray(_vertexArrayObject);
	}


	void MyRectangle::draw()
	{
//        glPushMatrix();
//        glTranslated(_position[0],_position[1],0);
//        glRotated(_angle,0,0,1);
//        glColor3f(0.8f,0.8f,0.2f);
//		glRectf(-20.0f,-10.0f,15.0f,10.0f);
//        glColor3f(0.5f,0.5f,0.2f);
//		glRectf(15.0f,-10.0f,20.0f,10.0f);
//        glPopMatrix();

		// binding the program shader
		glUseProgram(_program);

		// bindind the vertex data
		glBindBuffer(GL_ARRAY_BUFFER, _vertexBufferObject);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0); // feed 2 GL_FLOAT per vertex shader
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*)(sizeof(GLfloat)*2*12));

		// drawing the data in triangle form
		glDrawArrays(GL_TRIANGLES, 0, 12);

		// unbinding vertex data
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// unbinding program
		glUseProgram(0);
	}

	void MyRectangle::rotateDeg(glm::vec2 &vec, float angle)
	{
		glm::mat4 rotMat = glm::mat4(1.0f);
		rotMat = glm::rotate(rotMat, angle, glm::vec3(0.0f, 0.0f, 1.0f));
		glm::vec4 temp = rotMat * glm::vec4(vec, 0.0f, 1.0f);
		_direction[0] = temp[0];
		_direction[1] = temp[1];
	}

	void MyRectangle::update(float elapsed_millis)
	{
		float elapsed_seconds = elapsed_millis / 1000;

		if(cg::KeyBuffer::instance()->isCtrlActive())
		{
			if(cg::KeyBuffer::instance()->isSpecialKeyDown(GLUT_KEY_DOWN))
				_velocity -= _acceleration * elapsed_seconds;

			if(cg::KeyBuffer::instance()->isSpecialKeyDown(GLUT_KEY_UP))
				_velocity += _acceleration * elapsed_seconds;

			if(cg::KeyBuffer::instance()->isSpecialKeyDown(GLUT_KEY_RIGHT))
			{
				float delta = -_angularVelocity * elapsed_seconds;
				if(_velocity < 0 )
					delta *= -1;

				rotateDeg(_direction, delta);
				_angle += delta;
			}

			if(cg::KeyBuffer::instance()->isSpecialKeyDown(GLUT_KEY_LEFT))
			{
				float delta = _angularVelocity * elapsed_seconds;
				if(_velocity < 0 )
					delta *= -1;

				rotateDeg(_direction, delta);
				_angle += delta;
			}
		}
		_position += _direction * _velocity * elapsed_seconds;

		_modelMatrix = glm::mat4(1.0f);
		_modelMatrix = glm::translate( _modelMatrix, glm::vec3(_position, 0.0f));
		_modelMatrix = glm::rotate(_modelMatrix, _angle, glm::vec3(0.0f,0.0f,1.0f));


		glUseProgram(_program);
		glUniformMatrix4fv(_matUnif, 1, GL_FALSE, glm::value_ptr(_modelMatrix));
		glUseProgram(0);
	}

}
