#ifndef MY_CAMERA_H
#define MY_CAMERA_H

#include <string>
#include "cg/cg.h"

namespace lab2
{
	class MyTopCamera : public cg::Entity, public cg::IDrawListener, public cg::IReshapeEventListener
	{
		private:
			glm::mat4 _projectionMatrix;
			int _winWidth, _winHeight;
			GLuint _projectionMatrixUBO;

		public:
			MyTopCamera(const std::string& id);
			virtual ~MyTopCamera();
			void init();
			void draw();
			void onReshape(int width, int height);

			static const GLuint globalMatricesBindingIndex = 0;

	};
}

#endif
