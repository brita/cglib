project "cglib"
	kind "StaticLib"
	language "C++"
	files { "src/**.h", "src/**.cpp" }
	excludes { "src/cg/Manager_join.cpp" }
	targetdir "lib"
	targetname "cg"


	configuration "release"
		flags { "ExtraWarnings" , "Optimize" }
		objdir "obj/release";

	configuration "debug"
		flags { "ExtraWarnings" , "Symbols" }
		objdir "obj/debug";
		targetsuffix "D"
