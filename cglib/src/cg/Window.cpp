/*
 * Copyright 2013 Ines Almeida
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "Window.h"

#include "Macros.h"


namespace cg
{

	Window::Window()
	{
		//default values
		_x = 100;
		_y = 100;
		_width = 640;
		_height = 480;
		_display_mode = GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH;
		_caption = "OpenGL Application";
	}

	Window::~Window() {}

}
