/*
 * Copyright 2007 Carlos Martinho
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */



#ifndef RESHAPE_EVENT_NOTIFIER_H
#define RESHAPE_EVENT_NOTIFIER_H

#include "IReshapeEventListener.h"
#include "Notifier.h"
#include "Singleton.h"

namespace cg
{

	/** cg::ReshapeEventNotifier is a singleton notifier that maintains a list
	 *  of cg::IReshapeListener and controls the distribution of reshape events. 
	 *  When a reshape event reaches cg::ReshapeEventNotifier, it is dispatched 
	 *  to all previously registered and currently enabled cg::IReshapeListener's.
	 */
	class ReshapeEventNotifier : public Notifier<IReshapeEventListener>
	{

		SINGLETON_HEADER(ReshapeEventNotifier)
	
		public:
			void handleReshape(int width, int height);
			DUMP_METHOD(ReshapeEventNotifier)
	};
}

#endif // RESHAPE_EVENT_NOTIFIER_H
