/*
 * Copyright 2007 Carlos Martinho
 * Copyright 2013 Ines Almeida
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "Application.h"

namespace cg
{

	Application::Application()
	{
		_windowInfo._caption = "OpenGL Application";
		setup();
	}

	Application::Application(const std::string property_file)
	{
		setup();

		try	{
			cg::Properties::instance()->load(property_file);
		}
		catch(std::runtime_error& e)
		{
			DebugFile::instance()->writeException(e);
			throw e;
		}
	}

	inline
	void Application::setup()
	{
		_timeInfo.last = 0;
		_timeInfo.current = 0;
		_timeInfo.elapsed = 0;

		_isFirstUpdate = true;
	}

	Application::~Application()	{}


	const tTimeInfo& Application::getTimeInfo() const { return _timeInfo; }




	// Display

	void Application::onDisplay()
	{
		ViewNotifier::instance()->draw();
	}


	// Reshape

	void Application::onReshape(int w, int h)
	{
		BasicApplication::onReshape(w, h);
		ReshapeEventNotifier::instance()->handleReshape(w,h);
	}


	// Update

	inline
	void Application::updateFrameTime()
	{
		_timeInfo.last = _timeInfo.current;
		_timeInfo.current = Util::instance()->getAppTime()*1000.0f;
		_timeInfo.elapsed = _timeInfo.current - _timeInfo.last;
	}

	void Application::onUpdate()
	{
		Registry::instance()->emptyTrash();
		updateFrameTime();
		if(_isFirstUpdate)
		{
			_isFirstUpdate = false;
			return;
		}
		DebugNotifier::instance()->debug();
		UpdateNotifier::instance()->update((float)_timeInfo.elapsed);
	}



	// MOUSE

	void appMouseCallback(int button, int state, int x, int y)
	{
		MouseEventNotifier::instance()->handleMouse(button,state,x,y);
	}

	void appMotionCallback(int x, int y)
	{
		MouseEventNotifier::instance()->handleMouseMotion(x,y);
	}

	void appPassiveMotionCallback(int x, int y)
	{
		MouseEventNotifier::instance()->handleMousePassiveMotion(x,y);
	}


	// KEYBOARD

	void appKeyPressedCallback(unsigned char key, int x, int y)
	{
		KeyBuffer::instance()->pressKey(key);
		KeyboardEventNotifier::instance()->handleKeyPressed(key);
	}

	void appKeyReleasedCallback(unsigned char key, int x, int y)
	{
		KeyBuffer::instance()->releaseKey(key);
		KeyboardEventNotifier::instance()->handleKeyReleased(key);
	}

	void appSpecialKeyPressedCallback(int key, int x, int y)
	{
		KeyBuffer::instance()->pressSpecialKey(key);
		KeyboardEventNotifier::instance()->handleSpecialKeyPressed(key);
	}

	void appSpecialKeyReleasedCallback(int key, int x, int y)
	{
		KeyBuffer::instance()->releaseSpecialKey(key);
		KeyboardEventNotifier::instance()->handleSpecialKeyReleased(key);
	}



	void Application::onInit()
	{
		BasicApplication::onInit();

		// callbacks
		if( glutDeviceGet(GLUT_HAS_MOUSE) )
		{
			glutMouseFunc(appMouseCallback);
			glutMotionFunc(appMotionCallback);
			glutPassiveMotionFunc(appPassiveMotionCallback);
			//glutMouseWheelFunc
		}
		if( glutDeviceGet(GLUT_HAS_KEYBOARD) )
		{
			glutIgnoreKeyRepeat(GL_TRUE); //key repeat is switched off
			glutKeyboardFunc(appKeyPressedCallback);
			glutKeyboardUpFunc(appKeyReleasedCallback);
			glutSpecialFunc(appSpecialKeyPressedCallback);
			glutSpecialUpFunc(appSpecialKeyReleasedCallback);
		}


		try
		{
			createEntities();
			createViews();
		}
		catch(std::runtime_error& e)
		{
			DebugFile::instance()->writeException(e);
			throw e;
		}
		Registry::instance()->init();

	}




	void Application::addEntity(Entity *entity)
	{
		if(entity != 0)
			Registry::instance()->add(entity);
	}

	View* Application::createView(const std::string& id)
	{
		View* v = new View(id);
		if(v != 0)
			Registry::instance()->add(v);
		return v;
	}

	void Application::shutdown()
	{
		KeyboardEventNotifier::instance()->cleanup();
		MouseEventNotifier::instance()->cleanup();
		ReshapeEventNotifier::instance()->cleanup();
		UpdateNotifier::instance()->cleanup();
		DebugNotifier::instance()->cleanup();
		Registry::instance()->cleanup();
		DebugFile::instance()->cleanup();
		KeyBuffer::instance()->cleanup();
		Properties::instance()->cleanup();
		Util::instance()->cleanup();
		ViewNotifier::instance()->cleanup();
	}

	void Application::dump() const
	{
		Registry::instance()->dump();
		DebugFile::instance()->writeLine("[Notifiers]");
		UpdateNotifier::instance()->dump();
		KeyboardEventNotifier::instance()->dump();
		MouseEventNotifier::instance()->dump();
		ReshapeEventNotifier::instance()->dump();
		DebugNotifier::instance()->dump();
		DebugFile::instance()->newLine();
		ViewNotifier::instance()->dump();
	}

}
