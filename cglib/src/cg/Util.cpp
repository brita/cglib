/*
 * Copyright 2007 Carlos Martinho
 * Copyright 2013 Ines Almeida
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */



#include "Util.h"

#include <fstream>
#include <stdexcept>


namespace cg
{

	SINGLETON_IMPLEMENTATION(Util)

	void Util::drawBitmapString(std::string /*s*/, GLdouble /*x*/, GLdouble /*y*/)
	{
		fprintf(stderr, "drawBitmapString : empty function\n");
		/* TODO
        glRasterPos2d(x, y);
		std::string::iterator iend = s.end();
        for (std::string::iterator i = s.begin(); i != iend; i++) {
            glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, *i);
        }
        */
	}

	void Util::drawBitmapStringVector(std::vector<std::string> /*s*/, GLdouble /*x*/, GLdouble /*y*/, GLdouble /*delta*/)
	{
		fprintf(stderr, "drawBitmapStringVector : empty function\n");
		/* TODO
        std::vector<std::string>::iterator iend = s.end();
        for(std::vector<std::string>::iterator i = s.begin(); i != iend; i++) {
            drawBitmapString((*i),x,y);
            y += delta;
        }
		*/
	}

	double Util::getAppTime()
	{
		return glutGet(GLUT_ELAPSED_TIME)/1000.0f;
	}


	void Util::findFileOrThrow( const std::string &strFilename , std::ifstream &testFile)
	{
		testFile.open(strFilename.c_str());
		if(!testFile.is_open())
			throw std::runtime_error("Could not find the file " + strFilename);
	}
}
