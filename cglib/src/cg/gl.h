/*
 * Copyright 2012 Carlos Martinho
 * Copyright 2013 Ines Almeida
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */



#ifndef GL_H
#define GL_H

#include <iostream>


#ifdef GLEW
	#include "GL/glew.h"
#endif

#ifdef GLLOAD
	#include "glload/gl_3_3.h"
	#include "glload/gll.hpp" //contains functions to initialize glload
	// glload needs to be included *before* anything that includes GL/gl*.h headers.
#endif

//freeglut
#include "GL/freeglut.h"	// freeglut includes GL/gl.h


//GLM
#ifdef WIN32
	#pragma warning(push)
	#pragma warning(disable: 4201) // disable Visual Studio specific warnings
#endif
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/quaternion.hpp"
#include "glm/gtc/type_ptr.hpp"
#ifdef WIN32
	#pragma warning(pop)
#endif



#ifdef LOAD_X11
	#define APIENTRY
#endif

#endif // GL_H
