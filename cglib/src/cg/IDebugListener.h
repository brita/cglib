/*
 * Copyright 2007 Carlos Martinho
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef IDEBUG_LISTENER_H
#define IDEBUG_LISTENER_H

#include <fstream>

namespace cg
{

	/** cg::IDebugListener is the callback interface for the debug step.
	 */
	class IDebugListener
	{
		public:
			/** Automatically invoked by the DebugNotifier on each debug step.
			 *	Defines the callback's behavior.
			 *	\param	file	The debug file.
			 */
			virtual void debug(std::ofstream& file) = 0;
    };

}

#endif // IDEBUG_LISTENER_H
