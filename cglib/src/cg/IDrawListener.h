/*
 * Copyright 2007 Carlos Martinho
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef IDRAW_LISTENER_H
#define IDRAW_LISTENER_H

namespace cg
{
	/*
	 * cg::IDrawListener is the callback interface for the draw step.
	 */
	class IDrawListener
	{
		public:
			/*
			 * Automatically invoked by the DrawNotifier on each draw step.
			 * Defines the callback's behavior.
			 */
			virtual void draw() {}
			virtual void drawBlended() {}
			virtual void drawOverlay() {}
    };
}

#endif // IDRAW_LISTENER_H
