/*
 * Copyright 2013 Ines Almeida
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef BASICAPPLICATION_H_
#define BASICAPPLICATION_H_

#include "gl.h"
#include "Window.h"
#include "Manager.h"

namespace cg
{
	class Manager;

	void appDisplayCallback();
	void appUpdateCallback(int value);

	class BasicApplication
	{
		// glut callbacks can not be defined as member functions
		friend void appDisplayCallback();
		friend void appReshapeCallback(int width, int height);
		friend void appUpdateCallback(int value);

		protected:
			Window _windowInfo;
			int _intervalMillis;


		public:
			BasicApplication();
			virtual ~BasicApplication();

			Window* getWindowInfo();
			int getIntervalMillis();

			virtual void onInit();
			virtual void onDisplay();
			virtual void onReshape(int w, int h);
			virtual void onUpdate();

			virtual void shutdown();
	};

}


#endif // BASICAPPLICATION_H_
