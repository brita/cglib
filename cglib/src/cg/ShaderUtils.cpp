/*
 * Copyright 2013 Ines Almeida
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */



#include "ShaderUtils.h"

#include <algorithm>
#include <fstream>
#include <sstream>

namespace cg
{
	SINGLETON_IMPLEMENTATION(ShaderUtils)


	GLuint ShaderUtils::CreateShader(GLenum eShaderType, const char* strShaderFile)
	{
		GLuint shader = glCreateShader(eShaderType);
		glShaderSource(shader, 1, &strShaderFile, NULL);

		glCompileShader(shader);

		// retrieving compiler logs and status
		GLint infoLogLength;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);

		GLchar *strInfoLog = new GLchar[infoLogLength + 1];
		glGetShaderInfoLog(shader, infoLogLength, NULL, strInfoLog);

		const char *strShaderType = NULL;
		switch(eShaderType)
		{
			case GL_VERTEX_SHADER: strShaderType = "vertex"; break;
			case GL_GEOMETRY_SHADER: strShaderType = "geometry"; break;
			case GL_FRAGMENT_SHADER: strShaderType = "fragment"; break;
		}

		GLint status;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
		if (status == GL_TRUE)
		{
			std::cout << "Success compiling " << strShaderType << " shader." << std::endl << strInfoLog << std::endl;
		}
		else
		{
			std::cerr << "Compile failure in " << strShaderType << " shader:" << std::endl << strInfoLog << std::endl;
			glDeleteShader(shader);
		}

		delete[] strInfoLog;

		return shader;
	}


	GLuint ShaderUtils::LoadShader(GLenum eShaderType, const std::string &strShaderFilename)
	{
		std::ifstream shaderFile;
		Util::instance()->findFileOrThrow(strShaderFilename, shaderFile);
		std::stringstream shaderData;
		shaderData << shaderFile.rdbuf();
		shaderFile.close();

		return CreateShader(eShaderType, shaderData.str().c_str());
	}


	GLuint ShaderUtils::CreateProgram(const std::vector<GLuint> &shaderList)
	{
		GLuint program = glCreateProgram();

		for(size_t i=0; i<shaderList.size(); i++)
			glAttachShader(program, shaderList[i]);

		glLinkProgram(program);

		GLint status;
		glGetProgramiv (program, GL_LINK_STATUS, &status);
		if (status == GL_FALSE)
		{
			GLint infoLogLength;
			glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);

			GLchar *strInfoLog = new GLchar[infoLogLength + 1];
			glGetProgramInfoLog(program, infoLogLength, NULL, strInfoLog);
			std::cerr << "Linker failure: " << strInfoLog << std::endl;
			delete[] strInfoLog;
		}
		else
		{
			std::cout << "Program linked successfully" << std::endl;
		}

		for(size_t i=0; i<shaderList.size(); i++)
			glDetachShader(program, shaderList[i]);

		return program;
	}
}
