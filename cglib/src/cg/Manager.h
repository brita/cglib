/*
 * Copyright 2012 Carlos Martinho
 * Copyright 2013 Ines Almeida
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */



#ifndef MANAGER_H
#define MANAGER_H

#include "Macros.h"
#include "gl.h"
#include "DebugOGL.h"
#include "Singleton.h"
#include "BasicApplication.h"


namespace cg
{
	class BasicApplication;


	/** cg::Manager is a singleton class that connects a cg::Application to the OpenGL and GLUT callbacks.
	 *  To run a cg::Application, use the runApp method of cg::Manager.
	 *  From that moment on, all control of the application is based on callbacks.
	 *  Keyboard events are dispatched to cg::KeyboardEventNotifier and cg::Keybuffer.
	 *  Mouse events are dispatched to cg::MouseEventNotifier.
	 *  Reshape events are dispatched to cg::ReshapeEventNotifier.
	 *  cg::Application::update is called at the frame rate.
	 *  cg::Application::display is called after each update.
	 */

	class Manager
	{

		SINGLETON_HEADER(Manager)

		friend void appShutdownCallback();

		private:
			BasicApplication* _app;
			bool _isShutdown;
			void getOpenGLInfo();
			void loadOGLPointers(int window);


		public:
			BasicApplication* getApp();

			void runApp(BasicApplication *app, int glut_argc, char** glut_argv);

			/**
			 * Unregister callbacks and start the shutdown procedure.
			**/
			void shutdownApp();
	};

}

#endif // MANAGER_H

