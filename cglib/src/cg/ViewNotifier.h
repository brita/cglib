/*
 * Copyright 2012 Carlos Martinho
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */



#ifndef VIEW_NOTIFIER_H
#define VIEW_NOTIFIER_H

#include "IViewListener.h"
#include "Notifier.h"
#include "Singleton.h"

namespace cg
{

	class ViewNotifier : public Notifier<IViewListener>
	{
		SINGLETON_HEADER(ViewNotifier)

		public:
			virtual void draw();
			DUMP_METHOD(ViewNotifier)
	};
}

#endif // VIEW_NOTIFIER_H
