/*
 * Copyright 2007 Carlos Martinho
 * Copyright 2013 Ines Almeida
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */



#include "Manager.h"

namespace cg
{

	SINGLETON_IMPLEMENTATION_NO_CONSTRUCTOR(Manager)

	Manager::Manager()
	{
		_app = NULL;
		_isShutdown = false;
	}


	BasicApplication* Manager::getApp() { return _app; }


	void nullDisplayCallback() {}

	void appShutdownCallback()
	{
		std::cout << "Shutting down window and context ..." << std::endl;

		glutDisplayFunc(nullDisplayCallback);
		glutReshapeFunc(0);
		glutMouseFunc(0);
		glutMotionFunc(0);
		glutPassiveMotionFunc(0);
		glutKeyboardFunc(0);
		glutKeyboardUpFunc(0);
		glutSpecialFunc(0);
		glutSpecialUpFunc(0);
		glutCloseFunc(0);


		std::cout << "Shutting down the application ... " << std::endl;
		Manager::instance()->_app->shutdown();
		delete Manager::instance()->_app;
		Manager::instance()->cleanup();
		std::cout << "Done." << std::endl;
		exit(0);
	}



	void Manager::getOpenGLInfo()
	{
		/* Print OpenGL and Library Versions */
		#ifdef DEBUG
		checkOpenGLInfo(false);
		#endif

		/* OpenGL Debug Callback */
		if(glext_ARB_debug_output)
		{
			glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
			glDebugMessageCallbackARB(DebugFunc, (void*)15);
		}
		else
			std::cerr << "Extention 'ARB_debug_output' not available. OpenGL errors will be silent." << std::endl;
	}

	void Manager::loadOGLPointers(int window)
	{
		#ifdef GLEW
		std::cout << " using GLEW ..." << std::endl;
		glewExperimental = true;
		GLenum status = glewInit();
		if(status != GLEW_OK)
		{
			std::cerr << "Error glewInit failed: " << glewGetErrorString(status)) << std::endl;
			glutDestroyWindow(window);
			return;
		}
		#endif

		#ifdef GLLOAD
		std::cout << " using glload ..." << std::endl;
		if(glload::LoadFunctions() == glload::LS_LOAD_FAILED)
		{
			std::cerr << "Error loading OpenGL function pointers. Shutting down." << std::endl;
			glutDestroyWindow(window);
			return;
		}
		#endif
	}


	void Manager::runApp(BasicApplication *app, int glut_argc, char** glut_argv)
	{
		_app = app;
		Window win = * _app->getWindowInfo();

		int oglMaj = 3;
		int oglMin = 3;

		std::cout << "Using freeGLUT." << std::endl;
		glutInit(&glut_argc, glut_argv); //inits glut and a session with the window system. The arguments are window-system dependent


		/* OpenGL context setup */
		std::cout << "Setting OpenGL context for version " << oglMaj << "." << oglMin << " ..." << std::endl;
		glutInitContextVersion(oglMaj,oglMin);
		glutInitContextProfile(GLUT_CORE_PROFILE);
		#ifdef DEBUG
		glutInitContextFlags(GLUT_DEBUG);
		#endif


		/* Creating window */
		std::cout << "Creating the window ..." << std::endl;
		glutInitWindowPosition(win._x,win._y);
		glutInitWindowSize(win._width,win._height);
		int window = glutCreateWindow(win._caption.c_str());


		/* OpenGL function pointer loading. A valid OpenGL context is needed before initializing. */
		std::cout << "Loading OpenGL function pointers";
		loadOGLPointers(window);


		/* Print library versions and set OpenGL debug callback */
		getOpenGLInfo();


		/* Print OpenGL and Library Versions */
		#ifdef DEBUG
		checkOpenGLInfo(false);
		#endif


		/* OpenGL Debug Callback */
		if(glext_ARB_debug_output)
		{
			glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
			glDebugMessageCallbackARB(DebugFunc, (void*)15);
		}
		else
			std::cerr << "Extention 'ARB_debug_output' not available. OpenGL errors will be silent." << std::endl;

		/* Callback Setup */
		std::cout << "Setting up callbacks and initializing application ..." << std::endl;
		glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
		glutCloseFunc(appShutdownCallback);
		_app->onInit();
		_app->onReshape(win._width,win._height);


		/* Main Loop */
		std::cout << "Entering main loop ..." << std::endl;
		glutMainLoop();
	}


	void Manager::shutdownApp()
	{
		_isShutdown = true;
		glutLeaveMainLoop();
	}
}
