/*
 * Copyright 2012 Carlos Martinho
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */



#ifndef KEYBUFFER_H
#define KEYBUFFER_H

#include <cassert>
#include "gl.h"
#include "Singleton.h"

namespace cg
{

#define KEY_MAX 256
#define SPECIAL_KEY_MAX 118

	/** cg::Keybuffer maintains the current status of all keys of the keyboard.
	 *  At any time, this singleton can be queried to check if a key is pressed 
	 *  or released. This allows to detect multiple key combinations.
	 *  Special keys are the GLUT special keys (e.g. cursor and function keys).
	 */
	class KeyBuffer
	{

		SINGLETON_HEADER(KeyBuffer)

		private:
			bool _key[KEY_MAX];
			bool _specialKey[SPECIAL_KEY_MAX];
			char _modifierKeys;
			unsigned char modifyAlphaKey(unsigned char key);

		public:
			void pressKey(unsigned char key);
			void pressSpecialKey(int key);
			void releaseKey(unsigned char key);
			void releaseSpecialKey(int key);

			/** Checks if a key is down.
			 *	\param	key		The key's ASCII character.
			 *	\return			True if the given key is down, false otherwise.
			 */
			bool isKeyDown(unsigned char key) const;

			/** Checks if a key is up.
			 *	\param	key		The key's ASCII character.
			 *	\return			True if the given key is up, false otherwise.
			 */
			bool isKeyUp(unsigned char key) const;

			/** Checks if a special key is down.
		 	 *	\param	key		The GLUT_KEY_* constant for the special key.
		 	 *	\return			True if the given special key is down, false otherwise.
		 	 */
			bool isSpecialKeyDown(int key) const;

			/** Checks if a special key is up.
			 *	\param	key		The GLUT_KEY_* constant for the special key.
			 *	\return			True if the given special key is up, false otherwise.
			 */
			bool isSpecialKeyUp(int key) const;

			/** Checks if SHIFT was activated by the last key press.
			 *	\return			True if SHIFT is active, false otherwise.
			 */
			bool isShiftActive() const;

			/** Checks if CTRL was activated by the last key press.
			 *	\return			True if CTRL is active, false otherwise.
			 */
			bool isCtrlActive() const;

			/** Checks if ALT was activated by the last key press.
			 *	\return			True if ALT is active, false otherwise.
			 */
			bool isAltActive() const;
	};
}

#endif // KEYBUFFER_H
