/*
 * Copyright 2007 Carlos Martinho
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */



#ifndef IUPDATE_LISTENER_H
#define IUPDATE_LISTENER_H

namespace cg
{

	/** cg::IUpdateListener is the callback interface for update step.
	 */
	class IUpdateListener
	{
		public:
			/** At every interval given by the frame rate defined in Manager::runApp, an update event is
			 *	distributed by cglib to all registred Entities that implement the IUpdateListener interface.
			 *	For more details, please refer to glutTimerFunc in GLUT documentation.
			 *	\param	elapsed_millis	The event triggering interval (1000 / frame_rate).
			 */
			virtual void update(float elapsed_millis) = 0;
	};

}

#endif // IUPDATE_LISTENER_H
