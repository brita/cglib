/*
 * Copyright 2007 Carlos Martinho
 * Copyright 2013 Ines Almeida
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */



#include "View.h"

namespace cg {

	View::View(const std::string& id) : Entity(id)
	{
		_positionf = glm::vec2(0.0f,0.0f);
		_sizef = glm::vec2(1.0f,1.0f);
	}

	View::~View() {}


	inline
	IDrawListener* View::getDrawListener(const std::string& id)
	{
		Entity* entity = Registry::instance()->get(id);
		if(entity)
			return dynamic_cast<IDrawListener*>(entity);

		return 0;
	}


#define LINK_ENTITY(COMMAND,ID)\
	IDrawListener* dl = getDrawListener(ID);\
	if(dl) { _drawNotifier.COMMAND(dl); }\

#define LINK_ENTITY_AT(COMMAND,ID,ENTRYPOINT)\
	IDrawListener* dl = getDrawListener(ID);\
	if(dl) { _drawNotifier.COMMAND(dl,ENTRYPOINT); }\


	void View::linkEntityAtBeginning(std::string entity_id) { LINK_ENTITY(addAtBeginning,entity_id) }
	void View::linkEntityAtEnd(std::string entity_id) { LINK_ENTITY(addAtEnd,entity_id) }
	void View::linkEntityBefore(std::string entity_id, std::string entrypoint) { LINK_ENTITY_AT(addBefore,entity_id,entrypoint) }
	void View::linkEntityAfter(std::string entity_id, std::string entrypoint) { LINK_ENTITY_AT(addAfter,entity_id,entrypoint) }
	void View::unlinkEntity(std::string entity_id) { _drawNotifier.remove(entity_id); }


	void View::setClearColor(float r, float g, float b, float a)
	{
		_color = glm::vec4(r,g,b,a);
	}

	void View::setViewport(float x, float y, float width, float height)
	{
		_positionf = glm::vec2(x,y);
		_sizef = glm::vec2(width,height);
	}


	void View::onReshape(int width, int height)
	{
		glm::vec2 aux = glm::vec2(width, height);
		_positioni = _positionf * aux;
		_sizei = _sizef * aux;
	}

	void View::setOverlayProjection()
	{
		fprintf(stderr, "setOverlayProjection : empty function\n");
		/* TODO
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        gluOrtho2D(0,_sizei[0],0,_sizei[1]);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        */
	}


	void View::draw()
	{
		glScissor(_positioni[0],_positioni[1],_sizei[0],_sizei[1]);
		glEnable(GL_SCISSOR_TEST);

		glClearColor(_color[0], _color[1], _color[2], _color[3]);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glViewport(_positioni[0],_positioni[1],_sizei[0],_sizei[1]);

	//	glEnable(GL_DEPTH_TEST);
		_drawNotifier.draw();
/*
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
		_drawNotifier.drawBlended();

		glDisable(GL_DEPTH_TEST);
		//TODO setOverlayProjection();
		_drawNotifier.drawOverlay();

		glDisable(GL_BLEND);*/

		glDisable(GL_SCISSOR_TEST);
	}
}
