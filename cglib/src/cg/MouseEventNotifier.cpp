/*
 * Copyright 2007 Carlos Martinho
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */



#include "MouseEventNotifier.h"

namespace cg
{

	SINGLETON_IMPLEMENTATION(MouseEventNotifier)

    void MouseEventNotifier::handleMouse(int button, int button_state, int x, int y)
	{
		FOR_EACH_LISTENER(onMouse(button,button_state,x,y))
    }

    void MouseEventNotifier::handleMouseMotion(int x, int y)
	{
		FOR_EACH_LISTENER(onMouseMotion(x,y))
    }

    void MouseEventNotifier::handleMousePassiveMotion(int x, int y)
	{
		FOR_EACH_LISTENER(onMousePassiveMotion(x,y))
    }

}
