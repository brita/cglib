/*
 * Copyright 2007 Carlos Martinho
 * Copyright 2013 Ines Almeida
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */



#include "DebugFile.h"


namespace cg
{

	DebugFile* DebugFile::_instance = 0;

	DebugFile* DebugFile::instance()
	{
		if (_instance == 0)
		{
			_instance = new DebugFile(); 
		}
		return _instance;
	}


	DebugFile::DebugFile()
	{
		_file.open("cglib.log", std::fstream::out | std::fstream::app);
		_file << std::endl << "[log] " << Util::instance()->getAppTime() << std::endl;
	}

	DebugFile::~DebugFile() {}

	void DebugFile::cleanup()
	{
		_file.close();
		delete _instance;
	}



	std::ofstream& DebugFile::getOutputFileStream()
	{
		return _file;
	}

	void DebugFile::write(const std::string& s)
	{
		_file << s;
	}

	void DebugFile::writeLine(const std::string& s)
	{
		_file << s << std::endl;
	}

	void DebugFile::newLine()
	{
		_file << std::endl;
	}

	void DebugFile::writeException(std::runtime_error& e)
	{
		_file << "(EXCEPTION) " << e.what() << std::endl;
	}

}
