/*
 * Copyright 2007 Carlos Martinho
 * Copyright 2013 Ines Almeida
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */



#ifndef UTIL_H
#define UTIL_H

#include "Macros.h"
#include <string>
#include <vector>
#include "gl.h"
#include "Singleton.h"

namespace cg
{

	/** cg::Util is a singleton providing a set of utility methods.
	 */
	class Util
	{

		SINGLETON_HEADER(Util)

		public:
			/** Draws a given string on screen positioning it at the given OpenGL coordinates.
			 *	\param	s	The string to be drawn.
			 *	\param	x	The OpenGL x coordinate.
			 *	\param	y	The OpenGL y coordinate.
			 */
			void drawBitmapString(std::string s, GLdouble x, GLdouble y);

			/** Draws all strings in the given vector on screen, positioning the first string
			 *	at the given OpenGL coordinates. Every next string will be delta values
			 *	vertically distant from the previous one.
			 *	\param	s		The vector containing the strings to be drawn.
			 *	\param	x		The OpenGL x coordinate.
			 *	\param	y		The OpenGL y coordinate.
			 *	\param	delta	The value separating each string (-10 by default).
			 */
			void drawBitmapStringVector(std::vector<std::string> s, GLdouble x, GLdouble y, GLdouble delta = -10);

			/** Gets the current system time in milliseconds.
			 *	\return		The current system time.
			 */
			double getAppTime();


			void findFileOrThrow( const std::string &strFilename , std::ifstream &testFile);
	};

}

#endif // UTIL_H
