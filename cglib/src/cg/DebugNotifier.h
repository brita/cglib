/*
 * Copyright 2007 Carlos Martinho
 * Copyright 2013 Ines Almeida
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */



#ifndef DEBUG_NOTIFIER_H
#define DEBUG_NOTIFIER_H

#include "IDebugListener.h"
#include "Notifier.h"
#include "Singleton.h"

namespace cg {

	/** cg::DebugNotifier controls the debug step of the application, by
	 *  calling the cg::IDebugListener::debug method of all the previously 
	 *  registered and currently enabled cg::IDebugListener's.
	 */
	class DebugNotifier : public Notifier<IDebugListener> {

	SINGLETON_HEADER(DebugNotifier)
	public: 
		void debug();
		DUMP_METHOD(DebugNotifier)
	};
}

#endif // DEBUG_NOTIFIER_H
