/*
 * Copyright 2013 Ines Almeida
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "BasicApplication.h"


namespace cg
{
	BasicApplication::BasicApplication()
	{
		_windowInfo = * new Window();
		_windowInfo._caption = "Basic OpenGL Application";
		_intervalMillis = 1000 / 60;
	}

	BasicApplication::~BasicApplication() {}




	Window* BasicApplication::getWindowInfo() { return &_windowInfo; }
	int BasicApplication::getIntervalMillis() { return _intervalMillis; }



	// Display

	void appDisplayCallback()
	{
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		Manager::instance()->getApp()->onDisplay();

		glutSwapBuffers();
	}

	void BasicApplication::onDisplay() {}



	// Reshape

	void appReshapeCallback(int width, int height)
	{
		BasicApplication* theApp = Manager::instance()->getApp();
		theApp->_windowInfo._width = width;
		theApp->_windowInfo._height = height;

		theApp->onReshape(width, height);

		glutPostRedisplay();
	}

	void BasicApplication::onReshape(int w, int h) {}



	// Update

	void appUpdateCallback(int value)
	{
		BasicApplication* theApp = Manager::instance()->getApp();
		glutTimerFunc(theApp->getIntervalMillis(),appUpdateCallback,0);
		glutPostRedisplay();

		theApp->onUpdate();
	}

	void BasicApplication::onUpdate() {}



	// Shutdown

	void BasicApplication::shutdown()
	{
	}


	void BasicApplication::onInit()
	{
		glutDisplayFunc(appDisplayCallback);
		glutReshapeFunc(appReshapeCallback);
		glutTimerFunc(_intervalMillis,appUpdateCallback,0);
	}
}
