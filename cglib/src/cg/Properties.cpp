/*
 * Copyright 2007 Carlos Martinho
 * Copyright 2013 Ines Almeida
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include "Properties.h"

namespace cg
{

	const char Properties::COMMENT = '#';

	SINGLETON_IMPLEMENTATION(Properties)

	void Properties::parseLine(const std::string& line)
	{
		std::string name, value, separator;
		std::istringstream iss(line);
		if(line.size() > 0)
		{
			iss >> name >> separator;
			if(name[0] == COMMENT)
				return;

			value = iss.str().substr((unsigned int)(iss.tellg()));
			if(separator == "=")
			{
				size_t start = value.find_first_not_of(" ");
				size_t end = value.find_last_not_of(" ");
				std::pair<tPropertyIterator,bool> result = 
					_properties.insert( std::make_pair(name,value.substr(start,end-start+1)) );
				if(result.second == 0)
				{
					throw std::runtime_error("[cg::Properties] property '" + name + "' already exists.");
				}
			}
		}
	}


	void Properties::load(const std::string& filename)
	{
		std::ifstream file;
		Util::instance()->findFileOrThrow(filename, file);

		std::string line;
		while (!file.eof())
		{
			std::getline(file,line);
			parseLine(line);
		}
		file.close();
	}


	bool Properties::exists(const std::string& name)
	{
		tPropertyIterator i = _properties.find(name);
		return (i != _properties.end());
	}


	int Properties::getInt(const std::string& name)
	{
		std::string s = getString(name);
		std::istringstream iss(s);
		int i;
		if(iss >> i)
		{
			return i;
		}
		else
		{
			throw std::runtime_error("[cg::Properties] Cannot convert property '" + name + "' to <int> (value '"+ s +"').");
		}
	}


	float Properties::getFloat(const std::string& name)
	{
		std::string s = getString(name);
		std::istringstream iss(s);
		float f;
		if (iss >> f)
		{
			return f;
		}
		else
		{
			throw std::runtime_error("[cg::Properties] Cannot convert property '" + name + "' to <double> (value '"+ s +"').");
		}
	}


	double Properties::getDouble(const std::string& name)
	{
		std::string s = getString(name);
		std::istringstream iss(s);
		double d;
		if (iss >> d)
		{
			return d;
		}
		else
		{
			throw std::runtime_error("[cg::Properties] Cannot convert property '" + name + "' to <double> (value '"+ s +"').");
		}
	}


	std::string Properties::getString(const std::string& name)
	{
		tPropertyIterator i = _properties.find(name);
		if( i != _properties.end() )
		{
			return i->second;
		}
		else
		{
			throw std::runtime_error("[cg::Properties] unknown property '" + name + "'.");
		}
	}


	glm::ivec2 Properties::getVector2i(const std::string& name)
	{
		std::string s = getString(name);
		std::istringstream iss(s);
		int v0, v1;
		if(iss >> v0 >> v1)
		{
			return glm::ivec2(v0,v1);
		}
		else
		{
			throw std::runtime_error("[cg::Properties] Cannot convert property '" + name + "' to <ivec2> (value '"+ s +"').");
		}
	}


	glm::dvec2 Properties::getVector2d(const std::string& name)
	{
		std::string s = getString(name);
		std::istringstream iss(s);
		double v0, v1;
		if(iss >> v0 >> v1)
		{
			return glm::dvec2(v0,v1);
		}
		else
		{
			throw std::runtime_error("[cg::Properties] Cannot convert property '" + name + "' to <dvec2> (value '"+ s +"').");
		}
	}


	glm::ivec3 Properties::getVector3i(const std::string& name)
	{
		std::string s = getString(name);
		std::istringstream iss(s);
		int v0, v1, v2;
		if(iss >> v0 >> v1 >> v2)
		{
			return glm::ivec3(v0,v1,v2);
		}
		else
		{
			throw std::runtime_error("[cg::Properties] Cannot convert property '" + name + "' to <ivec3> (value '"+ s +"').");
		}
	}


	glm::dvec3 Properties::getVector3d(const std::string& name)
	{
		std::string s = getString(name);
		std::istringstream iss(s);
		double v0, v1, v2;
		if(iss >> v0 >> v1 >> v2)
		{
			return glm::dvec3(v0,v1,v2);
		}
		else
		{
			throw std::runtime_error("[cg::Properties] Cannot convert property '" + name + "' to <dvec3> (value '"+ s +"').");
		}
	}


	void Properties::clear()
	{
		_properties.clear();
	}

}

