/*
 * Copyright 2013 Ines Almeida
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */



#include "DebugOGL.h"

namespace cg
{

	DebugOGL::DebugOGL() {}
	DebugOGL::~DebugOGL() {}

	bool checkOpenGLInfo(const bool check_extensions)
	{
		std::cout << std::endl;

		std::cout << "OpenGL information and library versions:" << std::endl;

		// OpenGL
		std::cout << glGetString(GL_VENDOR) << " " << glGetString(GL_RENDERER) << std::endl;
		std::cout << "OpenGL v" << glGetString(GL_VERSION) << std::endl;
		std::cout << "GLSL v" << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

		// TODO
		if(check_extensions) {
			std::cout << glGetString(GL_EXTENSIONS) << std::endl;
		}

		// GLEW
		#ifdef GLEW
		std::cout << "GLEW v" << glewGetString(GLEW_VERSION) << std::endl;
		#endif

		// Freeglut
		int glut_version = glutGet(GLUT_VERSION);
		int glut_major = glut_version / 10000;
		int glut_minor = (glut_version -(glut_major*10000))/100;
		int glut_patch = glut_version -(glut_major*10000) -(glut_minor * 100);
		std::cout << "freeGLUT v" << glut_major << "." << glut_minor << "." << glut_patch << std::endl;

		// GLM
		std::cout << "GLM v" << GLM_VERSION_MAJOR << "." << GLM_VERSION_MINOR << "." << GLM_VERSION_PATCH
					<< "." << GLM_VERSION_REVISION << std::endl;

		// CGlib
		std::cout << "CGlib v" << CG_VERSION << std::endl;

		std::cout << std::endl;
		return true;
	}



	//TODO
	bool checkOpenGLError()
	{
		bool isError = false;
		/*GLenum errCode;
		const GLubyte *errString;
		while ((errCode = glGetError()) != GL_NO_ERROR) {
			isError = true;
			//TODO errString = gluErrorString(errCode);
			std::cerr << "OpenGL ERROR [" << errString << "]." << std::endl;
		}*/
		return isError;
	}

	void APIENTRY DebugFunc(GLenum source, GLenum type, GLuint id, GLenum severity,
								GLsizei length, const GLchar* message, GLvoid* userParam)
	{
		std::string srcName;
		switch(source)
		{
			case GL_DEBUG_SOURCE_API_ARB: srcName = "API"; break;
			case GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB: srcName = "Window System"; break;
			case GL_DEBUG_SOURCE_SHADER_COMPILER_ARB: srcName = "Shader Compiler"; break;
			case GL_DEBUG_SOURCE_THIRD_PARTY_ARB: srcName = "Third Party"; break;
			case GL_DEBUG_SOURCE_APPLICATION_ARB: srcName = "Application"; break;
			case GL_DEBUG_SOURCE_OTHER_ARB: srcName = "Other"; break;
		}

		std::string errorType;
		switch(type)
		{
			case GL_DEBUG_TYPE_ERROR_ARB: errorType = "Error"; break;
			case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB: errorType = "Deprecated Functionality"; break;
			case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB: errorType = "Undefined Behavior"; break;
			case GL_DEBUG_TYPE_PORTABILITY_ARB: errorType = "Portability"; break;
			case GL_DEBUG_TYPE_PERFORMANCE_ARB: errorType = "Performance"; break;
			case GL_DEBUG_TYPE_OTHER_ARB: errorType = "Other"; break;
		}

		std::string typeSeverity;
		switch(severity)
		{
			case GL_DEBUG_SEVERITY_HIGH_ARB: typeSeverity = "High"; break;
			case GL_DEBUG_SEVERITY_MEDIUM_ARB: typeSeverity = "Medium"; break;
			case GL_DEBUG_SEVERITY_LOW_ARB: typeSeverity = "Low"; break;
		}

		std::cerr << errorType.c_str() << " from " << srcName.c_str()
				  << ",\t" << typeSeverity.c_str() << " priority" << std::endl;
		std::cerr << "Message: " << message << std::endl;
	}
}
