/*
 * Copyright 2007 Carlos Martinho
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */



#ifndef KEYBOARD_EVENT_NOTIFIER_H
#define KEYBOARD_EVENT_NOTIFIER_H

#include "IKeyboardEventListener.h"
#include "Notifier.h"
#include "Singleton.h"

namespace cg
{

	/** cg::KeyboardEventNotifier is a singleton notifier that maintains a list
	 *  of cg::IKeyboardListener and controls the distribution of keyboard events. 
	 *  When a keyboard event reaches cg::KeyboardEventNotifier, it is dispatched 
	 *  to all previously registered and currently enabled cg::IKeyboardListener's.
	 */
	class KeyboardEventNotifier : public Notifier<IKeyboardEventListener>
	{

		SINGLETON_HEADER(KeyboardEventNotifier)
		public:
			void handleKeyPressed(unsigned char key);
			void handleKeyReleased(unsigned char key);
			void handleSpecialKeyPressed(int key);
			void handleSpecialKeyReleased(int key);
			DUMP_METHOD(KeyboardEventNotifier)

	};
}

#endif // KEYBOARD_EVENT_NOTIFIER_H
