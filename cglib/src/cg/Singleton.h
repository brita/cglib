/*
 * Copyright 2007 Carlos Martinho
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */



#ifndef SINGLETON_MACRO_H
#define SINGLETON_MACRO_H

/** Set of macros to implement singletons.
 */

#define SINGLETON_HEADER(CLASSNAME)\
	private: static CLASSNAME* _instance; CLASSNAME();\
	public: static CLASSNAME* instance(); virtual ~CLASSNAME(); void cleanup();

#define SINGLETON_IMPLEMENTATION(CLASSNAME)\
    CLASSNAME* CLASSNAME::_instance = 0;\
    CLASSNAME* CLASSNAME::instance() {\
        if (_instance == 0) { _instance = new CLASSNAME(); }\
        return _instance; }\
    CLASSNAME::CLASSNAME() {}\
    CLASSNAME::~CLASSNAME() {}\
	void CLASSNAME::cleanup() { if (_instance != 0) { delete _instance; _instance = 0; } }

#define SINGLETON_IMPLEMENTATION_NO_CONSTRUCTOR(CLASSNAME)\
    CLASSNAME* CLASSNAME::_instance = 0;\
    CLASSNAME* CLASSNAME::instance() {\
        if (_instance == 0) { _instance = new CLASSNAME(); }\
        return _instance; }\
    CLASSNAME::~CLASSNAME() {}\
	void CLASSNAME::cleanup() { if (_instance != 0) { delete _instance; _instance = 0; } }

#endif // SINGLETON_MACRO_H
