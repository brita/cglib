/*
 * Copyright 2007 Carlos Martinho
 *
 * This file is part of cglib.
 *
 * cglib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * cglib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with cglib. If not, see <http://www.gnu.org/licenses/>.
 *
 */



#ifndef UPDATE_NOTIFIER_H
#define UPDATE_NOTIFIER_H

#include "IUpdateListener.h"
#include "Notifier.h"
#include "Singleton.h"

namespace cg
{

	/** cg::UpdateNotifier controls the update step of the application, by
	 *  calling the cg::IUpdateListener::update method of all the previously 
	 *  registered and currently enabled cg::IUpdateListener's.
	 */
	class UpdateNotifier : public Notifier<IUpdateListener>
	{

		SINGLETON_HEADER(UpdateNotifier)

		public:
			void update(float elapsed_millis);
			DUMP_METHOD(UpdateNotifier)
	};
}

#endif // UPDATE_NOTIFIER_H
