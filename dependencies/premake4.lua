print "Premaking dependencies"

solution "dependencies"

	configurations {"Debug", "Release"}
		defines {"_CRT_SECURE_NO_WARNINGS", "_SCL_SECURE_NO_WARNINGS"}


	dofile("dependencies_names.lua")


	local scriptDir = "build_scripts/"

	if os.isdir(freeglut) then
		dofile(scriptDir.."freeglut.lua")
	end

	--if os.isdir(glfw) then
	--	dofile(scriptDir.."glfw.lua")
	--end

	--dofile("glew.lua")

	if os.isdir(glload) then
		dofile(scriptDir.."glload.lua")
	end

