newoption
{
	trigger = "fplib",
	value = "glload",
	description = "Choose a library to load OpenGL function pointers",
	allowed =
	{
		{"glload",		"OpenGL Load" },
		{"glew",		"OpenGL Extension Wrangler" }
	}
}
-- default
if not _OPTIONS["fplib"] then
	_OPTIONS["fplib"] = "glload"
end

	configuration "glload"
		defines "GLLOAD"

	configuration "glew"
		defines "GLEW"
