newoption
{
	trigger = "all",
	description = "Generate solutions for everything, including dependencies.",
}


-- A solution contains projects, and defines the available configurations
-- a solution is the workspace
solution "cglib"
	configurations { "debug", "release" }
		defines {"_CRT_SECURE_NO_WARNINGS", "_SCL_SECURE_NO_WARNINGS"}

	configuration "release"
		defines { "NDEBUG" }

	configuration "debug"
		defines { "DEBUG" }


	configuration "windows"
		defines { "GLUT_DISABLE_ATEXIT_HACK" }

	configuration "vs*"
		buildoptions { "/wd4100", } --disable vs specific warnings


	mainLibDir = "dependencies/"

	--setup dependencies
	dofile(mainLibDir.."dependencies_options.lua")



	options = ""
	for opt, val in pairs(_OPTIONS) do
		options = options.."--"..opt.."="..val.." "
	end

	args = ""
	for arg, val in pairs(_ARGS) do
		args = args..arg.."="..val.." "
	end


	--build dependencies
	configuration "all"
	if _OPTIONS["all"] then
		print "all - building dependencies"
		os.execute("premake4 --file="..mainLibDir.."premake4.lua ".._ACTION..args);
	end
	configuration {}


	dofile(mainLibDir.."dependencies_names.lua")

	configuration "glload"
		includedirs {mainLibDir..glload.."/include"}

	configuration "glew"
		includedirs {mainLibDir..glew.."/include"}

	configuration {}
		includedirs {mainLibDir..freeglut.."/include"}
		defines {"FREEGLUT_STATIC", "_LIB", "FREEGLUT_LIB_PRAGMAS=0"}
		includedirs {mainLibDir..glm}




--build cglib
dofile("cglib/build.lua")


--build samples
dofile("samples/build.lua")
--os.execute("premake4 --file=samples/premake4.lua ".._OPTIONS.._ACTION.._ARGS);

